const ordinanceForm = document.querySelector('#formValidate');
ordinanceForm.addEventListener('submit', (e) => {
    e.preventDefault();
    const violation_article = ordinanceForm['violation_article'].value;
    const violation_type = ordinanceForm['violation_type'].value;
    const violation_desc = ordinanceForm['violation_desc'].value;
    const violation_penalty = ordinanceForm['violation_penalty'].value;
    var flag = true;
    var dataArray = [violation_article, violation_type, violation_desc, violation_penalty];
    dataArray.forEach(data => {
        if (data == "")
            flag = false;
    })
    if (flag) {
        auth.onAuthStateChanged(user=>{
            // console.log(user.uid);
            db.collection('agency').doc(user.uid).collection('ordinance').add({
                violation_article : violation_article,
                violation_type : violation_type,
                violation_desc : violation_desc,
                violation_penalty : violation_penalty,
                status :"active",
                violation_dateCreated : new Date()
            }).then(()=>{
                swal(
                    "Success Adding", "New ordinance added", "success"
                );
                ordinanceForm.reset();
            }).catch((err)=>{
                swal(
                    err.message, "An Error occurred", "error"
                );
            });
        });
    } else {
        swal(
            "There are missing fields", "An Error occurred", "error"
        );
    }
});