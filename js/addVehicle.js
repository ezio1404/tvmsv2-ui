const vehicleForm = document.querySelector('#formValidate');
vehicleForm.addEventListener('submit', (e) => {
    e.preventDefault();
    const vehicle_plateNo = vehicleForm['vehicle_plateNo'].value;
    const vehicle_model = vehicleForm['vehicle_model'].value;
    const vehicle_brand = vehicleForm['vehicle_brand'].value;
    const vehicle_color = vehicleForm['vehicle_color'].value;
    const vehicle_type = vehicleForm['vehicle_type'].value;
    const vehicle_capacity = vehicleForm['vehicle_capacity'].value;
    const vehicle_regDateExpiry = vehicleForm['vehicle_regDateExpiry'].value;
    const vehicle_regDate = vehicleForm['vehicle_regDate'].value;
    const vehicle_status = vehicleForm['vehicle_status'].value;
    var flag = true;
    var dataArray = [vehicle_plateNo, vehicle_model, vehicle_brand, vehicle_color,
        vehicle_type, vehicle_capacity, vehicle_regDateExpiry, vehicle_regDate, vehicle_status,
    ];
    dataArray.forEach(data => {
        if (data == "")
            flag = false;
    })
    if (flag) {
        auth.onAuthStateChanged(user => {
            // console.log(user.uid);
            db.collection('agency').doc(user.uid).collection('vehicle').doc().set({
                vehicle_plateNo: vehicle_plateNo,
                vehicle_model: vehicle_model,
                vehicle_brand: vehicle_brand,
                vehicle_color: vehicle_color,
                vehicle_type: vehicle_type,
                vehicle_violationCount : parseFloat(1),
                vehicle_capacity: vehicle_capacity,
                vehicle_regDateExpiry: vehicle_regDateExpiry,
                vehicle_regDate: vehicle_regDate,
                vehicle_status: vehicle_status,
                vehicle_dateCreated: new Date()
            }).then(() => {
                swal(
                    "Success Adding", "New vehicle added", "success"
                );
                vehicleForm.reset();
            }).catch((err) => {
                swal(
                    err.message, "An Error occurred", "error"
                );
            });
        });
    } else {
        swal(
            "There are missing fields", "An Error occurred", "error"
        );
    }
});