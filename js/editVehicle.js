"use strict";
const vehicleForm = document.querySelector('#formValidate');
vehicleForm.addEventListener('submit',(e)=>{
    e.preventDefault();
    const user_uid = vehicleForm['user_uid'].value;
    const vehicle_uid = vehicleForm['vehicle_uid'].value;

    const vehicle_plateNo = vehicleForm['vehicle_plateNo'].value;
    const vehicle_brand = vehicleForm['vehicle_brand'].value;
    const vehicle_model = vehicleForm['vehicle_model'].value;
    const vehicle_type = vehicleForm['vehicle_type'].value;
    const vehicle_capacity = vehicleForm['vehicle_capacity'].value;
    const vehicle_color = vehicleForm['vehicle_color'].value;
    const vehicle_regDate = vehicleForm['vehicle_regDate'].value;
    const vehicle_regDateExpiry = vehicleForm['vehicle_regDateExpiry'].value;
    const vehicle_status = vehicleForm['vehicle_status'].value;
    

    var vehicleRef = db.collection('agency').doc(user_uid).collection('vehicle').doc(vehicle_uid);

    var flag = true;
    var dataArray = [vehicle_plateNo,vehicle_brand,vehicle_model,vehicle_type,
        vehicle_capacity,vehicle_color,vehicle_regDate,vehicle_regDateExpiry,vehicle_status];
    dataArray.forEach(data => {
        if (data == "")
            flag = false;
    });

    if (flag) {
        return vehicleRef.update({
            vehicle_plateNo: vehicle_plateNo,
            vehicle_model: vehicle_model,
            vehicle_brand: vehicle_brand,
            vehicle_color: vehicle_color,
            vehicle_type: vehicle_type,
            vehicle_capacity: vehicle_capacity,
            vehicle_regDateExpiry: vehicle_regDateExpiry,
            vehicle_regDate: vehicle_regDate,
            vehicle_status: vehicle_status,
            vehicle_updatedAt: new Date()
        }).then(() => {
            Swal.fire({
                title: 'Done updating',
                text: "You won't be able to revert this!",
                type: 'success',
                confirmButtonColor: '#3085d6',
                confirmButtonText: 'Redirect to List'
              }).then((result) => {
                if (result.value) {
                  window.location.href="vehicle.html?message=updated"
                }
              })
        }).catch((err) => {
            swal(
                err.message, "An Error occurred", "error"
            );
        });
    } else {
        swal(
            "Field missing", "An Error occurred", "error"
        );
    }
})