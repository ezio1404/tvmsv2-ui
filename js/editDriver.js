const driverForm = document.querySelector('#formValidate');

driverForm.addEventListener('submit', (e) => {
    e.preventDefault();
    const user_uid = driverForm['user_uid'].value;
    const driver_uid = driverForm['driver_uid'].value;
    const license_no = driverForm['license_no'].value;
    const driver_fname = driverForm['driver_fname'].value;
    const driver_lname = driverForm['driver_lname'].value;
    const driver_mi = driverForm['driver_mi'].value;
    const driver_addr = driverForm['driver_addr'].value;
    const driver_city = driverForm['driver_city'].value;
    const driver_postal = driverForm['driver_postal'].value;
    const driver_phone = driverForm['driver_phone'].value;
    const status = driverForm['status'].value;

    var driverRef = db.collection('agency').doc(user_uid).collection('driver').doc(driver_uid);

    var flag = true;
    var dataArray = [license_no,driver_uid, driver_fname, driver_lname, driver_addr, driver_city, driver_postal, driver_phone];
    dataArray.forEach(data => {
        if (data == "")
            flag = false;
    });

    if (flag) {
        return driverRef.update({
            license_no: license_no,
            driver_fname: driver_fname,
            driver_lname: driver_lname,
            driver_mi: driver_mi,
            driver_addr: driver_addr,
            driver_updatedAt: new Date(),
            driver_city: driver_city,
            driver_postal: driver_postal,
            driver_phone: driver_phone,
            status:status
        }).then(() => {
            Swal.fire({
                title: 'Done updating',
                text: "You won't be able to revert this!",
                type: 'success',
                confirmButtonColor: '#3085d6',
                confirmButtonText: 'Redirect to List'
              }).then((result) => {
                if (result.value) {
                  window.location.href="driver.html?message=updated"
                }
              })
        }).catch((err) => {
            swal(
                err.message, "An Error occurred", "error"
            );
        });
    } else {
        swal(
            "Field missing", "An Error occurred", "error"
        );
    }

});