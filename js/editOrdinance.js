const violationForm = document.querySelector('#formValidate');

violationForm.addEventListener('submit', (e) => {
    e.preventDefault();
    const user_uid = violationForm['user_uid'].value;
    const violation_uid = violationForm['violation_uid'].value;

    const violation_article = violationForm['violation_article'].value;
    const violation_desc = violationForm['violation_desc'].value;
    const violation_type = violationForm['violation_type'].value;
    const violation_penalty = violationForm['violation_penalty'].value;
    const status = violationForm['status'].value;
    

    var hotlineRef = db.collection('agency').doc(user_uid).collection('ordinance').doc(violation_uid);

    var flag = true;
    var dataArray = [violation_article,violation_uid, violation_desc, violation_type,violation_penalty,status];
    dataArray.forEach(data => {
        if (data == "")
            flag = false;
    });

    if (flag) {
        return hotlineRef.update({
            violation_article: violation_article,
            violation_desc: violation_desc,
            violation_penalty: violation_penalty,
            violation_type: violation_type,
            violation_updatedAt : new Date(),
            status: status
        }).then(() => {
            Swal.fire({
                title: 'Done updating',
                text: "You won't be able to revert this!",
                type: 'success',
                confirmButtonColor: '#3085d6',
                confirmButtonText: 'Redirect to List'
              }).then((result) => {
                if (result.value) {
                  window.location.href="ordinance.html?message=updated"
                }
              })
        }).catch((err) => {
            swal(
                err.message, "An Error occurred", "error"
            );
        });
    } else {
        swal(
            "Field missing", "An Error occurred", "error"
        );
    }

});