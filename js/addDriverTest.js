const driverForm = document.querySelector('#formValidate');
const secondaryApp = firebase.initializeApp(firebaseConfig, "secondApp");
const secondaryAuth = secondaryApp.auth();

driverForm.addEventListener('submit', (e) => {
    e.preventDefault();
    const driver_fname = driverForm['driver_fname'].value;
    const driver_lname = driverForm['driver_lname'].value;
    const driver_mi = driverForm['driver_mi'].value;
    const driver_addr = driverForm['driver_addr'].value;
    const driver_city = driverForm['driver_city'].value;
    const driver_postal = driverForm['driver_postal'].value;
    const driver_bday = driverForm['driver_bday'].value;
    const driver_phone = driverForm['driver_phone'].value;
    const driver_email = driverForm['driver_email'].value;
    const driver_password = driverForm['driver_password'].value;
    const license_expiry = driverForm['license_expiry'].value;
    const license_no = driverForm['license_no'].value;
    const license_restriction = driverForm['license_restriction'].value;
    const license_agency = driverForm['license_agency'].value;
    var flag = true;
    var dataArray = [driver_fname, driver_lname, driver_addr, driver_city, driver_postal, driver_bday, driver_phone, driver_email, license_expiry, license_no, license_restriction, license_agency];
    dataArray.forEach(data => {
        if (data == "")
            flag = false;
    })

    if (flag) {
        auth.onAuthStateChanged(user => {
            db.collection("agency")
                .onSnapshot(function (snapshot) {
                    snapshot.docChanges().forEach(function (change) {
                        if (change.type === "added") {
                            db.collection("agency").doc(change.doc.id).collection("driver")
                                .onSnapshot(function (snapshot) {
                                    snapshot.docChanges().forEach(function (change) {
                                        if (change.type === "added") {
                                            // console.log("driver: ", change.doc.data().driver_email, " licenseNO : ",change.doc.data().license_no);
                                            if(change.doc.data().driver_email != driver_email && license_no != change.doc.data().license_no){
                                                // console.log("both is unqiue");
                                                secondaryAuth.createUserWithEmailAndPassword(driver_email, driver_password).then((driver) => {
                                                    db.collection('agency').doc(user.uid).collection('driver').doc(driver.user.uid).set({
                                                        driver_fname: driver_fname,
                                                        driver_lname: driver_lname,
                                                        driver_mi: driver_mi,
                                                        driver_addr: driver_addr,
                                                        driver_city: driver_city,
                                                        driver_bday: driver_bday,
                                                        driver_phone: driver_phone,
                                                        driver_violationCount: parseFloat(1),
                                                        driver_email: driver_email,
                                                        license_expiry: license_expiry,
                                                        license_no: license_no,
                                                        license_restriction: license_restriction,
                                                        license_agency: license_agency,
                                                        driver_dateCreated: new Date(),
                                                        status: "driver"
                                                    }).then(() => {
                                                        secondaryAuth.auth().signOut();
                                                        swal(
                                                            "Success Adding", "New driver added", "success"
                                                        );
                                                        driverForm.reset();
                                                    });
                                                }).catch((err) => {
                                                    console.log(err);
                                                    swal(
                                                        err.message, "An Error occurred", "error"
                                                    );
                                                });        
                                            }else{
                                                // console.log("one is the same");
                                            }
                                            
                                        }
                                    });
                                }, function (error) {
                                    swal(
                                        error.message, "An Error occurred", "error"
                                    );
                                });
                        }
                    });
                }, function (error) {
                    swal(
                        error.message, "An Error occurred", "error"
                    );
                });
        });
    } else {
        swal(
            "There are missing fields", "An Error occurred", "error"
        );
    }
});

