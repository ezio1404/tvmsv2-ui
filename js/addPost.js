'use strict';
const postForm = document.querySelector('#formValidate');
const btnAddPost =  document.querySelector('#btnAddPost');
btnAddPost.addEventListener('click', (e) => {
    e.preventDefault();
    const post_title = postForm['post_title'].value;
    const post_content = postForm['post_content'].value;
    const post_date = postForm['post_date'].value;
    // image
    let image = document.querySelector('#imageBtn').files[0];
    let imageName = image.name;
    let user = auth.currentUser;
    console.log(user.uid);
    let storageRef = storage.ref(user.uid + '/post/' + imageName);
    let uploadTask = storageRef.put(image);
    uploadTask.on('state_changed', snapshot => {
        let percentage = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
        console.log("Upload :" + percentage);
    },err => {
        console.log(err);
        swal(
            err.message, "An Error occurred", "error"
        );
    }, () => {
        uploadTask.snapshot.ref.getDownloadURL().then((downloadURL) => {
            let user = auth.currentUser;
            console.log(downloadURL);
            db.collection("agency").doc(user.uid).collection('post').add({
                post_title: post_title,
                post_content: post_content,
                post_date: post_date,
                post_dateCreated: new Date(),
                post_dateUpdated: "",
                post_status: "active",
                post_photoRef: downloadURL
            }).then(() => {
                swal(
                    "Posting Success", "New Traffic Added",
                    "success"
                );
                console.log(downloadURL);
                $('#modal1').closeModal({
                    dismissible: true
                });
            }).catch((err) => {
                console.log(err.message);
            });

        });
    });
})