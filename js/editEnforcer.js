const enforcerForm = document.querySelector('#formValidate');

enforcerForm.addEventListener('submit', (e) => {
    e.preventDefault();
    const user_uid = enforcerForm['user_uid'].value;
    const enforcer_uid = enforcerForm['enforcer_uid'].value;
    const enforcer_fname = enforcerForm['enforcer_fname'].value;
    const enforcer_lname = enforcerForm['enforcer_lname'].value;
    const enforcer_mi = enforcerForm['enforcer_mi'].value;
    const enforcer_addr = enforcerForm['enforcer_addr'].value;
    const enforcer_city = enforcerForm['enforcer_city'].value;
    const enforcer_postal = enforcerForm['enforcer_postal'].value;
    const enforcer_phone = enforcerForm['enforcer_phone'].value;

    var enforcerRef = db.collection('agency').doc(user_uid).collection('enforcer').doc(enforcer_uid);

    var flag = true;
    var dataArray = [enforcer_uid, enforcer_fname, enforcer_lname, enforcer_addr, enforcer_city, enforcer_postal, enforcer_phone];
    dataArray.forEach(data => {
        if (data == "")
            flag = false;
    });

    if (flag) {
        return enforcerRef.update({
            enforcer_fname: enforcer_fname,
            enforcer_lname: enforcer_lname,
            enforcer_mi: enforcer_mi,
            enforcer_addr: enforcer_addr,
            enforcer_city: enforcer_city,
            enforcer_postal: enforcer_postal,
            enforcer_updatedAt : new Date,
            enforcer_phone: enforcer_phone,
        }).then(() => {
            Swal.fire({
                title: 'Done updating',
                text: "You won't be able to revert this!",
                type: 'success',
                confirmButtonColor: '#3085d6',
                confirmButtonText: 'Redirect to List'
              }).then((result) => {
                if (result.value) {
                  window.location.href="enforcer.html?message=updated"
                }
              })
        }).catch((err) => {
            swal(
                err.message, "An Error occurred", "error"
            );
        });
    } else {
        swal(
            "Field missing", "An Error occurred", "error"
        );
    }

});