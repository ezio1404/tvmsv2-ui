const adminItems = document.querySelectorAll('.admin')

auth.onAuthStateChanged(user => {

    if (user) {
        user.getIdTokenResult().then(idTokenResult => {
            user.admin = idTokenResult.claims;

            if (user.admin.admin == true) {
                window.location.href = "admin_dashboard.html"
            }
        });

  
        const email = document.querySelector('.user-roal');
        email.innerHTML = user.email;

        const profileDisplayName = document.querySelector('.profile-btn');
        profileDisplayName.innerHTML = user.displayName;

        const profilePicture = document.querySelector('.profile-image');
        profilePicture.setAttribute("src", user.photoURL);


        const emailVerify = document.querySelector('#emailVerify');
        if (!user.emailVerified) {
            emailVerify.innerHTML = `Reminder : This <strong>${user.email}</strong> is not 
    verified. You can't use the features unless verified.Go to profile to send verification.`;
        } else {

            const card = document.querySelector('#card-alert');
            card.setAttribute('class', 'card green lighten-5');
            emailVerify.setAttribute('class', 'card-content green-text');
            emailVerify.innerHTML = `This email is verified.`;
            adminItems.forEach(item => item.style.display = "block");


            console.log(user.admin);


        }
    } else {
        window.location.href = "login.html"
    }


});


const logoutBtn = document.querySelector('#logout');
logoutBtn.addEventListener('click', (e) => {
    e.preventDefault();
    auth.signOut().then(() => {

    });
});