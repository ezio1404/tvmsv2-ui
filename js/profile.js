auth.onAuthStateChanged(user => {
    if (user) {
        if (user.emailVerified) {
            let emailVerify = document.querySelector('.email-verify-link');
            emailVerify.style.display = "none"
        }
        db.collection('agency').doc(user.uid).onSnapshot(doc => {
            const display_name = document.querySelector('#display_name');
            display_name.value = user.displayName;
            const profileDp = document.querySelector('#blah');
            profileDp.setAttribute('src', doc.data().agency_profileRef);

            const subPhoto = document.querySelector('#blah2');
            subPhoto.setAttribute('src', doc.data().subscription_photoRef);


            addSubsBtn = document.querySelector('#addSubsBtn');
            var today = new Date();
            var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
            console.log(date);
            if (doc.data().subscription_end == date || doc.data().subscription_end == "") {
                addSubsBtn.style.display = "inline-block";
            } else {
                addSubsBtn.style.display = "none";
            }


            document.querySelector('#email_verified').innerHTML = user.emailVerified ? "Verified ✔" : "Not Verified";
            document.querySelector('#subscription_status').innerHTML = doc.data().subscription_status == "active" ? "Paid ✔" : "Not Paid";

            editForm['agency_name'].value = document.querySelector('#agency_name').innerHTML = doc.data().agency_name;
            editForm['agency_head'].value = document.querySelector('#agency_head').innerHTML = doc.data().agency_head;
            editForm['agency_addr'].value = document.querySelector('#agency_addr').innerHTML = doc.data().agency_addr;
            editForm['agency_city'].value = document.querySelector('#agency_city').innerHTML = doc.data().agency_city;
            editForm['agency_postal'].value = document.querySelector('#agency_postal').innerHTML = doc.data().agency_postal;
            editForm['agency_landline'].value = document.querySelector('#agency_landline').innerHTML = doc.data().agency_landline;
            editForm['agency_phone'].value = document.querySelector('#agency_phone').innerHTML = doc.data().agency_phone;
            document.querySelector('#agency_geopoint').innerHTML = doc.data().agency_geopoint.latitude + "N " + doc.data().agency_geopoint.longitude + "E";
            editForm['agency_lat'].value = doc.data().agency_geopoint.latitude;
            editForm['agency_long'].value = doc.data().agency_geopoint.longitude;

            editProfile(user);
        }, function (error) {
            swal(
                error.message, "An error occured", "error"
            );
        });


    } else {
        window.location.href = "login.html"
    }
});
const linkVerify = document.querySelector('#linkVerify');
linkVerify.addEventListener('click', (e) => {
    e.preventDefault();
    let user = firebase.auth().currentUser;
    user.sendEmailVerification().then(function () {
        swal(
            "Verification sent", "Please check your email", "success"
        );
    }).catch(function (error) {
        swal(
            error.message, "An error occured", "error"
        );
    });
})
const editForm = document.querySelector('#formValidate');

function editProfile(user) {
    editForm.addEventListener('submit', (e) => {
        // add some trapping
        e.preventDefault();
        db.collection("agency").doc(user.uid).update({
            agency_name: editForm['agency_name'].value,
            agency_head: editForm['agency_head'].value,
            agency_addr: editForm['agency_addr'].value,
            agency_city: editForm['agency_city'].value,
            agency_lat: parseFloat(editForm['agency_lat'].value),
            agency_long: parseFloat(editForm['agency_long'].value),
            agency_postal: editForm['agency_postal'].value,
            agency_phone: editForm['agency_phone'].value,
            agency_landline: editForm['agency_landline'].value,
            agency_geopoint: new firebase.firestore.GeoPoint(parseFloat(editForm['agency_lat'].value), parseFloat(editForm['agency_long'].value))
        }).then(() => {
            swal(
                "Success updating", "Updated Agency Info added", "success"
            );
            $('#modal1').closeModal({
                dismissible: true
            });
        }).catch((err) => {
            swal(
                err.message, "An error occured", "error"
            );
        });

    })
}