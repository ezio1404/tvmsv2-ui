const enforcerForm = document.querySelector('#formValidate');
const secondaryApp = firebase.initializeApp(firebaseConfig,"secondApp");
const secondaryAuth = secondaryApp.auth();

enforcerForm.addEventListener('submit', (e) => {
    e.preventDefault();
    const enforcer_fname = enforcerForm['enforcer_fname'].value;
    const enforcer_lname = enforcerForm['enforcer_lname'].value;
    const enforcer_mi = enforcerForm['enforcer_mi'].value;
    const enforcer_addr = enforcerForm['enforcer_addr'].value;
    const enforcer_city = enforcerForm['enforcer_city'].value;
    const enforcer_postal = enforcerForm['enforcer_postal'].value;
    const enforcer_bday = enforcerForm['enforcer_bday'].value;
    const enforcer_phone = enforcerForm['enforcer_phone'].value;
    const enforcer_email = enforcerForm['enforcer_email'].value;
    const enforcer_password = enforcerForm['enforcer_password'].value;
    var flag = true;
    var dataArray = [enforcer_fname, enforcer_lname, enforcer_addr,enforcer_city,enforcer_postal,enforcer_bday,enforcer_phone,enforcer_email];
    dataArray.forEach(data => {
        if (data == "")
            flag = false;
    })
    if (flag) {
        auth.onAuthStateChanged(user=>{
            secondaryAuth.createUserWithEmailAndPassword(enforcer_email,enforcer_password).then((enforcer)=>{
                db.collection('agency').doc(user.uid).collection('enforcer').doc(enforcer.user.uid).set({
                    enforcer_fname : enforcer_fname,
                    enforcer_lname : enforcer_lname,
                    enforcer_mi : enforcer_mi,
                    enforcer_addr : enforcer_addr,
                    enforcer_city : enforcer_city,
                    enforcer_postal : enforcer_postal,
                    enforcer_bday : enforcer_bday,
                    enforcer_phone : enforcer_phone,
                    enforcer_email : enforcer_email,
                    enforcer_dateCreated : new Date(),
                    status: "enforcer"
                }).then(()=>{
                    swal(
                        "Success Adding", "New enforcer added", "success"
                    );
                    enforcerForm.reset();
                    secondaryAuth.auth().signOut();
                }).catch((err)=>{
                    // swal(
                    //     err.message, "An Error occurred", "error"
                    // );
                });
            }).then(()=>{
                swal(
                    "Success Adding", "New enforcer added", "success"
                );
            });
        });
    } else {
        swal(
            "There are missing fields", "An Error occurred", "error"
        );
    }
});