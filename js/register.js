const agencyForm = document.querySelector('#formValidate');
// const agency_avatar = document.querySelector('#agency_avatar');
agencyForm.addEventListener('submit', (e) => {
    e.preventDefault();
    const agency_name = agencyForm['agency_name'].value;
    const agency_head = agencyForm['agency_head'].value;
    const agency_addr = agencyForm['agency_addr'].value;
    const agency_city = agencyForm['agency_city'].value;
    const agency_postal = agencyForm['agency_postal'].value;
    const agency_lat = agencyForm['agency_lat'].value;
    const agency_long = agencyForm['agency_long'].value;
    const agency_phone = agencyForm['agency_phone'].value;
    const agency_email = agencyForm['agency_email'].value;
    const agency_landline = agencyForm['agency_landline'].value;
    const agency_password = agencyForm['agency_password'].value;
    var flag = true;
    var dataArray = [agency_name, agency_head, agency_addr, agency_city, agency_postal, agency_lat, agency_long, agency_phone, agency_landline, agency_password];
    dataArray.forEach(data => {
        if (data == "")
            flag = false;
    })

    if (flag) {
        auth.createUserWithEmailAndPassword(agency_email, agency_password).then(cred => {
            
                db.collection('agency').doc(cred.user.uid).set({
                    agency_email: agency_email,
                    agency_name: agency_name,
                    agency_head: agency_head,
                    agency_addr: agency_addr,
                    agency_city: agency_city,
                    subscription_end: "",
                    agency_lat: parseFloat(agency_lat),
                    agency_long: parseFloat(agency_long),
                    agency_postal: agency_postal,
                    agency_geopoint: new firebase.firestore.GeoPoint(parseFloat(agency_lat), parseFloat(agency_long)),
                    agency_phone: agency_phone,
                    agency_landline: agency_landline,
                    subscription_status: "inactive",
                    agency_dateCreated: new firebase.firestore.Timestamp.fromDate(new Date())
                }).then(() => {
                    // swal(
                    //     "Success creating", "please verify your email to use the features", "success"
                    // );
                    agencyForm.reset();
                    window.location.href ="dashboard.html"
                }).catch(err => {
                    swal(
                        err.message, "An Error occurred", "error"
                    );
                });


        }).catch(err => {
            swal(
                err.message, "An Error occurred", "error"
            );
        });
    } else {
        swal(
            "There are missing fields", "An Error occurred", "error"
        );
    }

});